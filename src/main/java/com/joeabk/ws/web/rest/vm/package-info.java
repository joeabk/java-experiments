/**
 * View Models used by Spring MVC REST controllers.
 */
package com.joeabk.ws.web.rest.vm;
